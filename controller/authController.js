const Usuario = require("../models/usersModel");
const htmlspecialchars = require('htmlspecialchars');
const bcrypt = require('bcrypt');

exports.getLogin = (req, res) => {
    if(req.session.ident){
        res.redirect('/');
    } else {
        
        res.render("login", { title: "Login" });
    }
    
}

exports.auth = (req, res) => {
    let data;
    Usuario.findByName({ username: htmlspecialchars(req.fields.username) }) // busca usuario
        .then((resp) => { // si se ejecuta correctamente la query
            let usuario = resp[0];
            if (usuario != null) {
                if (usuario.active == 1) {
                    /**
                     * Compara password
                     */
                    
                    bcrypt.compare(req.fields.password, usuario.pass, function (err, resp) {
                        if (resp == true) {
                            req.session.name = usuario.name;
                            req.session.username = usuario.username;
                            req.session.ident = usuario.id;
                            console.log(req.session.ident)
                            res.send({type:"success", redirect:"/perfil"});
                        } else {
                            res.send({type:"error", redirect:"/login",message:"Error usuario/contraseña" });
                        }
                    });
                } else {
                    res.send({type:"error", redirect:"/login",message:"Error usuario/contraseña" })
                }
            } else {
                res.send({type:"error", redirect:"/login",message:"Error usuario/contraseña" });
            }
        })
        .catch(err => res.send({type:"error", redirect:"/login",message:err })) // si genera un error la query
}
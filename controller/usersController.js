const Usuario = require("../models/usersModel");
const htmlspecialchars = require('htmlspecialchars');
const Moment = require('moment-timezone');
const bcrypt = require('bcrypt');
const saltRounds = 10;

/**
 * renderiza vista con formulario
 */
exports.createUser = (req, res) => {
    if(req.session.ident){
        res.redirect('/');
    } else {
        res.render("usuarios/registra", { title: "Registrar" })
        
    }
    
}
/**
 * guarda los datos enviados del formulario
 */
exports.saveUser = (req, res) => {
    let nDate = Moment().tz('America/Matamoros').format();
    let data;
    Usuario.findByName({ username: htmlspecialchars(req.fields.username) }) // busca usuario
        .then((resp) => { // si se ejecuta correctamente la query
            if (resp[0] == null) { // si la cadena devuelta esta vacia guarda
                bcrypt.hash(req.fields.password, saltRounds, function (err, hash) {
                    data = {
                        name: htmlspecialchars(req.fields.name),
                        username: htmlspecialchars(req.fields.username),
                        pass: hash,
                        user_create: nDate,
                        created_at: nDate,
                        updated_at: nDate
                    };
                    Usuario.save(data)
                        .then(() => {
                            res.send({class: "alert-success", message: "Guardado"});
                        })
                        .catch(err => {
                            res.send(err)
                        });
                });
            } else { // si no esta vacia la cadena el usuario ya esta tomado
                res.send({class:"alert-info", message: `Usuario ${resp[0].username} ya existe`});
            }
        })
        .catch(err => res.send(err)) // si genera un error la query
}


const db = require("../db");
const table = "tbl_users";
/**
 * Guarda los datos de usuario
 */
exports.save = (data) => {
    return new Promise((resolve, reject) => {
        db.query(`INSERT INTO ${table} SET ?`, data, function (error, results, fields) {
            if (error)  reject(error);
            console.log(results);
            resolve();
        });
    })
}

/**
 * Busca todos los usuario por nombre
 */
exports.findByName = (data) => {
    return new Promise((resolve, reject) => {
        db.query(`SELECT * FROM ${table} WHERE username = '${data.username}'`, function (error, results, fields) {
            if (error)  reject(error);
            resolve(results);
        });
    })
}

const app = document.querySelector("#my_app");

app.addEventListener("click", el => {
    if (el.target.getAttribute("id") == "btn_create_user") {
        let form = document.getElementById("reg_user");
        let data = new FormData(form);
        let nameInput = document.querySelector("[name = 'name']").value;
        let usernameInput = document.querySelector("[name = 'username']").value;
        let passInput = document.querySelector("[name = 'password']").value;
        new Promise((resolve, reject) => {
            if ((passInput == null || passInput.length == 0 || /^\s+$/.test(passInput))
                || (nameInput == null || nameInput.length == 0 || /^\s+$/.test(nameInput))
                || (usernameInput == null || usernameInput.length == 0 || /^\s+$/.test(usernameInput))) {
                reject("Todos los campos son necesarios");
            } else {
                resolve();

            }
        })
            .then(() => {
                let form = document.getElementById("reg_user");
                let data = new FormData(form);
                post("/usuario/nuevo", data)
                    .then((received) => {
                        let res = JSON.parse(received);
                        document.getElementById("messages").innerHTML = `<div class = 'alert ${res.class}'>${res.message}</div>`;
                        form.reset();
                    })
                    .catch(err => {
                        document.getElementById("messages").innerHTML = `<div class = 'alert alert-danger'>${err}</div>`;
                        form.reset();
                    })
            })
            .catch(err => {
                document.getElementById("messages").innerHTML = `<div class = 'alert alert-danger'>${err}</div>`;
                form.reset();
            })

        /*
        */
    }
    if (el.target.getAttribute("id") == "btn_login") {
        let form = document.getElementById("log_in");
        let data = new FormData(form);
        post("/login", data)
            .then((received) => {
                let rec = JSON.parse(received);
                if (rec.type=="success"){
                    window.location.href = rec.redirect;
                }
                if (rec.type=="error"){
                    document.getElementById('messages').innerHTML = `<div class ='alert alert-danger'>${rec.message}</div>`;
                    form.reset();
                }
            })
            .catch(err => {
                
                form.reset();
            })
    }
})
const express = require("express");
const router = express.Router();
const index = require("../controller/indexController");
const users = require("../controller/usersController");
const auth = require("../controller/authController");
/**
 * method get
 */
router.get("/", index.getIndex);
router.get("/usuario/nuevo", users.createUser);
router.get("/login", auth.getLogin);
router.get("/perfil", (req, res) => {
    if (req.session.ident) {
        res.render('usuarios/perfil', { title: 'Ap', name: req.session.name, ident: req.session.ident, username: req.session.username });
    } else {
        res.render('login', { title: 'Login' });
    }
});
router.get('/logout', function (req, res, next) {
    if (req.session) {
        // delete session object
        req.session.destroy(function (err) {
            if (err) {
                return next(err);
            } else {
                return res.redirect('/');
            }
        });
    }
});
/**
 * method post
 */
router.post("/usuario/nuevo", users.saveUser);

router.post("/login", auth.auth);
module.exports = router;
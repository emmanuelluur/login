CREATE DATABASE IF NOT EXISTS login_app DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE login_app;

CREATE TABLE IF NOT EXISTS tbl_users
(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, -- primary key column
    name VARCHAR(60),
    username varchar(60),
    pass text NOT NULL,
    user_create DATE,
    active TINYINT(1) DEFAULT 1,
    created_at DATETIME,
    updated_at DATETIME
    -- specify more columns here
)ENGINE=InnoDB DEFAULT CHARSET=utf8;